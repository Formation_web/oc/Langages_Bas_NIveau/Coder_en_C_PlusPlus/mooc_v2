#include "math.h"
#include <iostream>

using namespace std;

/*
 * Fonction qui ajoute 2 au nombre recu en argu�ent
 *-nbRecu : Le nombre auquel la fonction ajoute 2
 * Valeur retourn�: nbRecu + 2
 */
int ajouteDeux(int nbRecu)
{
    int valeur(nbRecu + 2);
    //On cr�e une case en m�moire
    //On prend le nombre re�u en argument, on lui ajoute 2
    //Et on met tout cela dans la m�moire

    return valeur;
}

int addition(int a, int b) {
    return a + b;
}

int multiplication(double a, double b, double c) {
    return a * b * c;
}

// ==================================== //
//------------> EXEMPLES <------------- //
// ==================================== //

double carre(double x)
{
    double resultat;
    resultat = x * x;

    return resultat;
}

void dessineRectangle(int l, int h)
{
    for (int ligne(0); ligne < h; ligne++)
    {
        for (int col(0); col < l; col++)
        {
            std::cout << "*";
        }
        std::cout << endl;
    }
}

