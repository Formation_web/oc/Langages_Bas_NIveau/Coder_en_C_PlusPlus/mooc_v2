#include "functions.h"
#include <iostream>

using namespace std;


// Declaration des variable

double choiceNumberOne()
{
    double operande1;
    std::cout << "Entrez le premier operande" << endl;
    std::cin >> operande1; // Demande au user le premier operande

    return operande1;
}

double choiceNumberTwo()
{
    double operande2;
    std::cout << "Entrez le second operande" << endl;
    std::cin >> operande2; // Demande au user le second operande

    return operande2;
}

int choiceOperator()
{
    std::cout << "=======================================" << endl;
    std::cout << "Selectionnez maintenant le chiffre correspondant a l'operation que vous souhaiter effectuer :" << endl;
    std::cout << "1 ---> Addition  :" << endl;
    std::cout << "2 ---> Soustraction :" << endl;
    std::cout << "3 ---> Multiplication :" << endl;
    std::cout << "4 ---> Division :" << endl;
    std::cout << "=======================================" << endl;

    int operatore;

    std::cin >> operatore;

    return operatore;
}



