#include <iostream>

using namespace std;


int main()
{
	// ################################# //
	// ----------> Calculatrice <------  //
	// ################################# //

	// Affichage intro
	std::cout << "|================================================================|" << endl;
	std::cout << "|----------->Bienvenue dans notre calculatrice basic <-----------|" << endl;
	std::cout << "|----------------------------------------------------------------|" << endl;
	std::cout << "|-> Tous d'abords vous aller entrer les 2 operandes a calculer <-|" << endl;
	std::cout << "|================================================================|" << endl;

	// Declaration des variable
	double operande1, operande2, resultat;
	int operation;

	std::cout << "Entrez le premier operande" << endl;
	std::cin >> operande1; // Demande au user le premier operande

	std::cout << "Entrez le second operande" << endl;
	std::cin >> operande2; // Demande au user le second operande

    std::cout << "|==================================================================================|" << endl;
	std::cout << "|--> Selectionnez maintenant le chiffre correspondant a l'operation souhaiter : <--|" << endl;
	std::cout << "|--> 1 ---> Addition  : <----------------------------------------------------------|" << endl;
	std::cout << "|--> 2 ---> Soustraction : <-------------------------------------------------------|" << endl;
	std::cout << "|--> 3 ---> Multiplication : <-----------------------------------------------------|" << endl;
	std::cout << "|--> 4 ---> Division : <-----------------------------------------------------------|" << endl;
	std::cout << "|==================================================================================|" << endl;

	// Chois de l'operation a effectuer
	std::cin >> operation;

	// AFFICHAGE DU RESULTAT
	switch (operation)
	{
        case 1:
            std::cout << operande1 << " + " << operande2 << " = " << operande1 + operande2;
            break;
        case 2:
            std::cout << operande1 << " - " << operande2 << " = " << operande1 - operande2;
            break;
        case 3:
            std::cout << operande1 << " x " << operande2 << " = " << operande1 * operande2;
            break;
        case 4:
            std::cout << operande1 << " / " << operande2 << " = " << operande1 / operande2;
            break;
	}

	return 0;
}
